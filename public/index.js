/* demo cosmetics */
const menuPad = (state) => {
  if (state) {
    document.querySelector('main').classList.add('with-menu');
  } else {
    document.querySelector('main').classList.remove('with-menu');
  }
};


/* dynamic configurator */
const mutate = (componentOrTemplate, prop, newVal) => {
  const promoter = (componentOrTemplate.shadowRoot || componentOrTemplate).querySelector('#promote');
  if (promoter == null) return;

  switch(prop) {
    case 'view':
      promoter.checked = newVal === 'menu';
      break;
    case 'locked':
      promoter.disabled = newVal === 'true';
      break;
    default:
  }

  let locked = promoter.disabled,
    isMenuView = promoter.checked;

  // console.log('locked:', locked, 'menu:', isMenuView);
  menuPad(isMenuView && locked); // if locked and is a menu: enable padding, otherwise - set default padding
}


/* z-aside definition */
class ZAside extends HTMLElement {

  constructor() {
    super();
    this.view = 'sidebar'; // "sidebar" (collapsed) or "menu" (full-sized)
    this.locked = 'false'; // "true" (allow) or "false" (prohibit) view changes
  }

  static get observedAttributes() {
    return ['view', 'locked'];
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: 'open' }),
      template = document.getElementById('z-aside').content.cloneNode(true),
      zaView = this.view,
      zaLocked = this.locked;

    /*
      Note: properties are not being monitored from HTML (changing them after page is loaded does nothing)
    */

    // Array.from( template.querySelectorAll('.za-view') )
    //   .forEach( n => n.textContent = zpView );

    if (zaView) {
      // template.querySelector('#promote').checked = zaView === 'menu';
      mutate(template, 'view', zaView);
    }

    if (zaLocked === 'true') {
      // template.querySelector('#promote').setAttribute('disabled', 'disabled');
      mutate(template, 'locked', zaLocked);
    }

    shadow.append( template );
  }

  attributeChangedCallback(property, oldValue, newValue) {
    if (oldValue === newValue) return;

    mutate(this, property, newValue);
    this[ property ] = newValue;
  }
}
customElements.define( 'z-aside', ZAside );
