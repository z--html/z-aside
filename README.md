# z-aside Web Component

Sidebar / overlay menu / menu configurable custom web component.

[![pipeline status](https://gitlab.com/z--html/z-aside/badges/main/pipeline.svg)](https://gitlab.com/z--html/z-aside/-/commits/main)
Live demo: [z--html.gitlab.io/z-aside/](https://z--html.gitlab.io/z-aside/).
